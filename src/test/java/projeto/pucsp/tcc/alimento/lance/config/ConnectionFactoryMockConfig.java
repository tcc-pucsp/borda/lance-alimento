package projeto.pucsp.tcc.alimento.lance.config;

import com.rabbitmq.client.Channel;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class ConnectionFactoryMockConfig {

    @Bean
    public ConnectionFactory connectionFactory() {

        final ConnectionFactory connectionFactory = Mockito
                .mock(ConnectionFactory.class);

        final Connection connection = connection();

        Mockito
                .when(connectionFactory.createConnection())
                .thenReturn(connection);

        Mockito
                .when(connection.isOpen())
                .thenReturn(true);

        Mockito
                .when(connection.createChannel(Mockito.anyBoolean()))
                .thenReturn(Mockito.mock(Channel.class));
        
        return connectionFactory;

    }

    private Connection connection() {

        return Mockito.mock(Connection.class);

    }

}
