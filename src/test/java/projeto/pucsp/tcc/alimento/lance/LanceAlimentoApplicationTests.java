package projeto.pucsp.tcc.alimento.lance;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LanceAlimentoApplicationTests {

    private static final String CAMINHO_ALIMENTOS = "/alimentos";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String URI_PUBLICAR = "/publicar";
    private static final String HEADER_ID_SOCIAL = "id-social";
    private static final String HEADER_EMAIL_SOCIAL = "email-social";
    private static final String VALOR_HEADER_EMAIL = "email@email.com";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    @Autowired
    private ApplicationContext context;

    private WebTestClient client;

    private ClientAndServer clientAndServer;

    private final ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void setup() {

        final int porta = 1080;

        clientAndServer = new ClientAndServer(porta);

        client = WebTestClient
                .bindToApplicationContext(this.context)
                .configureClient()
                .build();

    }

    @Test
    public void publicarLanceAlimentoComRetornoDeServicoAlimentoOkay() throws IOException {


        try (InputStream inputStream = classLoader.getResourceAsStream("alimentos.json")) {

            String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

            clientAndServer
                    .when(request().withMethod("GET").withPath(CAMINHO_ALIMENTOS))
                    .respond(response().withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()));

            client
                    .post()
                    .uri(URI_PUBLICAR)
                    .body(Flux.just(
                            new Alimento(2, 1010),
                            new Alimento(4, 1010),
                            new Alimento(8, 1010)), Alimento.class)
                    .header(HEADER_ID_SOCIAL, "1")
                    .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                    .header(LATITUDE, "-485.58627")
                    .header(LONGITUDE, "-47.475235")
                    .exchange()
                    .expectStatus().isCreated();

        }


    }

    @Test
    public void publicarLanceAlimentoComRetornoDeServicoAlimentoOkPoremParaUmCodigoInvalido() throws IOException {

        try (InputStream inputStream = classLoader.getResourceAsStream("alimentos.json")) {

            String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

            clientAndServer
                    .when(request().withMethod("GET").withPath(CAMINHO_ALIMENTOS))
                    .respond(response().withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()));

            client
                    .post()
                    .uri(URI_PUBLICAR)
                    .body(Flux.just(
                            new Alimento(1, 1010),
                            new Alimento(4, 1010)), Alimento.class)
                    .header(HEADER_ID_SOCIAL, "1")
                    .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                    .header(LATITUDE, "-485.58627")
                    .header(LONGITUDE, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isNotFound()
                    .expectBody()
                    .json("{\"mensagem\":\"[1, 4]\",\"notificacao\":{\"codigo\":404,\"contexto\":\"Código(s) da lista alimento não encontrado(s)\"}}\n");
        }


    }



    @Test
    public void publicarLanceAlimentoComRetornoDeServicoAlimentoRetornando404() throws IOException {

        try (InputStream inputStream = classLoader.getResourceAsStream("alimento-nao-encontrado.json")) {

            String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

            clientAndServer
                    .when(request().withMethod("GET").withPath(CAMINHO_ALIMENTOS))
                    .respond(response().withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.NOT_FOUND.value()));

            client
                    .post()
                    .uri(URI_PUBLICAR)
                    .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                    .header(HEADER_ID_SOCIAL, "1")
                    .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                    .header(LATITUDE, "-485.58627")
                    .header(LONGITUDE, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isNotFound()
                    .expectBody()
                    .json("{\n" +
                            "    \"mensagem\": \"\",\n" +
                            "    \"notificacao\": {\n" +
                            "        \"codigo\": 404,\n" +
                            "        \"contexto\": \"Código(s) da lista alimento não encontrado(s)\"\n" +
                            "    }\n" +
                            "}");
        }

    }

    @Test
    public void publicarLanceAlimentoComRetornoDeServicoIndisponivel() {

        clientAndServer.stop();

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBody()
                .json("{\"mensagem\":\"Serviço de Alimento indisponível. Tente novamente mais tarde\",\"notificacao\":{\"codigo\":500,\"contexto\":\"Serviço Indisponível\"}}\n");


    }

    @Test
    public void publicarLanceAlimentoSemIdSocial() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoSemEmailSocial() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoSemLatitude() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoSemLongitude() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, 1010)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoComBodyFaltandoCodigo() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(null, 1010)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoComBodyFaltandoQuantidade() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, null)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest();

    }

    @Test
    public void publicarLanceAlimentoComBodyQuantidadeNegativa() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, -4)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":5045,\"contexto\":\"Existe(m) incosistência de informações\"}");

    }

    @Test
    public void publicarLanceAlimentoComBodyCodigoNegativo() {

        client
                .post()
                .uri(URI_PUBLICAR)
                .body(Flux.just(new Alimento(1, -4)), Alimento.class)
                .header(HEADER_ID_SOCIAL, "1")
                .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                .header(LATITUDE, "-485.58627")
                .header(LONGITUDE, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":5045,\"contexto\":\"Existe(m) incosistência de informações\"}");

    }

    @Test
    public void publicarLanceAlimentoComRetornoDeServicoAlimentoOkPoremParaUmAlimentoNaColecaoComCodigoNegativo() throws IOException {

        try (InputStream inputStream = classLoader.getResourceAsStream("alimentos.json")) {

            String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

            clientAndServer
                    .when(request().withMethod("GET").withPath(CAMINHO_ALIMENTOS))
                    .respond(response().withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()));

            client
                    .post()
                    .uri(URI_PUBLICAR)
                    .body(Flux.just(
                            new Alimento(2, 1010),
                            new Alimento(-4, 1010)), Alimento.class)
                    .header(HEADER_ID_SOCIAL, "1")
                    .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                    .header(LATITUDE, "-485.58627")
                    .header(LONGITUDE, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isBadRequest()
                    .expectBody()
                    .jsonPath("notificacao", "{\"codigo\":5045,\"contexto\":\"Existe(m) incosistência de informações\"}");
        }


    }

    @Test
    public void publicarLanceAlimentoComRetornoDeServicoAlimentoOkPoremParaUmAlimentoNaColecaoComQuantidadeNegativa() throws IOException {

        try (InputStream inputStream = classLoader.getResourceAsStream("alimentos.json")) {

            String result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8);

            clientAndServer
                    .when(request().withMethod("GET").withPath(CAMINHO_ALIMENTOS))
                    .respond(response().withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()));

            client
                    .post()
                    .uri(URI_PUBLICAR)
                    .body(Flux.just(
                            new Alimento(2, -1010),
                            new Alimento(4, 1010)), Alimento.class)
                    .header(HEADER_ID_SOCIAL, "1")
                    .header(HEADER_EMAIL_SOCIAL, VALOR_HEADER_EMAIL)
                    .header(LATITUDE, "-485.58627")
                    .header(LONGITUDE, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isBadRequest()
                    .expectBody()
                    .jsonPath("notificacao", "{\"codigo\":5045,\"contexto\":\"Existe(m) incosistência de informações\"}");
        }


    }

    @After
    public void stopMockServer() {
        clientAndServer.stop();
    }

}
