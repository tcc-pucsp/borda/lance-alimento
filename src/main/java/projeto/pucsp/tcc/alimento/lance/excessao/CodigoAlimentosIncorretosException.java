package projeto.pucsp.tcc.alimento.lance.excessao;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import projeto.pucsp.tcc.alimento.lance.entidade.Resposta;
import projeto.pucsp.tcc.alimento.lance.enumeracao.Notificacao;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Getter
public class CodigoAlimentosIncorretosException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 2046594156859723526L;

    private final transient Mono<Resposta> resposta;

    private final transient HttpStatus httpStatus;

    public CodigoAlimentosIncorretosException(ClientResponse clientResponse) {
        resposta = Mono.just(Resposta.RESPOSTA_CODIGO_ALIMENTO_INCORRETO);
        httpStatus = clientResponse.statusCode();
    }

    public CodigoAlimentosIncorretosException(List<Integer> codigos) {
        httpStatus = HttpStatus.NOT_FOUND;
        resposta = Mono.just(
                new Resposta(
                        Notificacao.CODIGO_ALIMENTO_INCORRETO.getCodigo(),
                        Notificacao.CODIGO_ALIMENTO_INCORRETO.getContexto(),
                        codigos.toString()));
    }
}
