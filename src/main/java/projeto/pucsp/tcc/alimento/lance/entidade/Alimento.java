package projeto.pucsp.tcc.alimento.lance.entidade;

import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Value
public class Alimento {

    @NotNull
    @Min(0)
    Integer codigo;

    @NotNull
    @Min(0)
    Integer quantidade;

    public Boolean valido() {
        return Optional.ofNullable(codigo).orElse(0) > 0 && Optional.ofNullable(quantidade).orElse(0) > 0;
    }
}
