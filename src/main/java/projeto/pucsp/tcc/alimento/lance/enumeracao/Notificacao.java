package projeto.pucsp.tcc.alimento.lance.enumeracao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Notificacao {

    SERVICO_ALIMENTO_INDISPONIVEL(500, "Serviço Indisponível", "Serviço de Alimento indisponível. Tente novamente mais tarde"),
    CODIGO_ALIMENTO_INCORRETO(404, "Código(s) da lista alimento não encontrado(s)", ""),
    ALIMENTO_INCORRETO(5045, "Existe(m) incosistência de informações",
            "Alguma informação é incosistente. Verifique os valores e tente novamente");

    Integer codigo;

    String contexto;

    String mensagem;

}
