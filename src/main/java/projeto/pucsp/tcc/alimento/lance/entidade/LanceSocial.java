package projeto.pucsp.tcc.alimento.lance.entidade;

import java.util.List;

import lombok.Value;

@Value
public class LanceSocial {

	String emailSocial;

	Integer idSocial;

	List<Alimento> alimentos;

	Localizacao localizacao;

}
