package projeto.pucsp.tcc.alimento.lance;

import java.util.List;

import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import reactor.core.publisher.Mono;

public interface RecursoLanceAlimento {

	Mono<Void> publicarLance(Integer idSocial, String emailSocial, String latitude, String longitude, List<Alimento> lanceAlimentos);

}
