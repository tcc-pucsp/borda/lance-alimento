package projeto.pucsp.tcc.alimento.lance.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.alimento.lance.RecursoLanceAlimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.entidade.LanceSocial;
import projeto.pucsp.tcc.alimento.lance.entidade.Localizacao;
import projeto.pucsp.tcc.alimento.lance.propriedade.PropriedadeLanceAlimento;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
class ServicoLanceAlimento implements RecursoLanceAlimento {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeLanceAlimento propriedadeLanceAlimento;

    public ServicoLanceAlimento(RabbitTemplate rabbitTemplate, PropriedadeLanceAlimento propriedadeLanceAlimento) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeLanceAlimento = propriedadeLanceAlimento;
    }

    @Override
    public Mono<Void> publicarLance(Integer idSocial, String emailSocial, String latitude, String longitude, List<Alimento> lanceAlimentos) {

        log.info("Publicar Lance : {} para {}", lanceAlimentos, propriedadeLanceAlimento);

        return Mono.fromRunnable(() ->

                rabbitTemplate.convertAndSend(propriedadeLanceAlimento.getTopicExchange(),
                        new LanceSocial(emailSocial, idSocial, lanceAlimentos, new Localizacao(latitude, longitude)))

        );

    }

}
