package projeto.pucsp.tcc.alimento.lance.excessao;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Resposta;
import projeto.pucsp.tcc.alimento.lance.enumeracao.Notificacao;
import reactor.core.publisher.Mono;

import java.util.List;

@Getter
public class AlimentosIncorretosException extends RuntimeException {

    private final transient Mono<Resposta> resposta;

    private final transient HttpStatus httpStatus;

    public AlimentosIncorretosException(List<Alimento> alimentos) {

        httpStatus = HttpStatus.BAD_REQUEST;
        resposta = Mono.just(
                new Resposta(
                        Notificacao.ALIMENTO_INCORRETO.getCodigo(),
                        Notificacao.ALIMENTO_INCORRETO.getContexto(),
                        alimentos.toString()));

    }
}

