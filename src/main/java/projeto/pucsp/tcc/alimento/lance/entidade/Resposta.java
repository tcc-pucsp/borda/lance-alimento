package projeto.pucsp.tcc.alimento.lance.entidade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Value;
import projeto.pucsp.tcc.alimento.lance.enumeracao.Notificacao;

@Data
public class Resposta {

    public static final Resposta RESPOSTA_SERVICO_ALIMENTO_INDISPONIVEL = new Resposta(Notificacao.SERVICO_ALIMENTO_INDISPONIVEL);
    public static final Resposta RESPOSTA_CODIGO_ALIMENTO_INCORRETO = new Resposta(Notificacao.CODIGO_ALIMENTO_INCORRETO);

    @JsonProperty("notificacao")
    private RespostaNotificacao respostaNotificacao;

    private String mensagem;

    private Resposta(Notificacao notificacao) {

        respostaNotificacao = new RespostaNotificacao(notificacao.getCodigo(), notificacao.getContexto());

        mensagem = notificacao.getMensagem();

    }

    public Resposta(Integer codigo, String contexto, String mensagem) {

        respostaNotificacao = new RespostaNotificacao(codigo, contexto);

        this.mensagem = mensagem;

    }

    @Value
    private static
    class RespostaNotificacao {

        Integer codigo;

        String contexto;

    }
}
