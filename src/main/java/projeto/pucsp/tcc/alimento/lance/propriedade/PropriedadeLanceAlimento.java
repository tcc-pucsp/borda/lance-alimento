package projeto.pucsp.tcc.alimento.lance.propriedade;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@ConfigurationProperties(prefix = "rabbit")
@Validated
public class PropriedadeLanceAlimento {

	private String topicExchange;

	private String key;

}
