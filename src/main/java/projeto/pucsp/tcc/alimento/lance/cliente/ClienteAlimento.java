package projeto.pucsp.tcc.alimento.lance.cliente;

import org.springframework.web.reactive.function.client.ClientResponse;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.excessao.CodigoAlimentosIncorretosException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ClienteAlimento {

	Flux<Alimento> obterTodosAlimentosPorCodigos(List<Integer> codigos);

	default Mono<RuntimeException> responseError(ClientResponse clientResponse) {
		throw new CodigoAlimentosIncorretosException(clientResponse);
	}
}
