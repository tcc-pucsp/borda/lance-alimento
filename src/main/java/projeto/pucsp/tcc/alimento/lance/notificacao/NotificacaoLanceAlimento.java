package projeto.pucsp.tcc.alimento.lance.notificacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import projeto.pucsp.tcc.alimento.lance.entidade.Resposta;
import projeto.pucsp.tcc.alimento.lance.excessao.AlimentosIncorretosException;
import projeto.pucsp.tcc.alimento.lance.excessao.CodigoAlimentosIncorretosException;
import projeto.pucsp.tcc.alimento.lance.excessao.ServicoAlimentoIndisponivelException;
import reactor.core.publisher.Mono;

@Slf4j
@RestControllerAdvice
public class NotificacaoLanceAlimento {

    @ExceptionHandler(CodigoAlimentosIncorretosException.class)
    public ResponseEntity<Mono<Resposta>> codigoDeAlimentosIncorretos(CodigoAlimentosIncorretosException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getResposta());
    }

    @ExceptionHandler(ServicoAlimentoIndisponivelException.class)
    public ResponseEntity<Mono<Resposta>> servicoAlimentoIndisponivel(ServicoAlimentoIndisponivelException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getResposta());
    }

    @ExceptionHandler(AlimentosIncorretosException.class)
    public ResponseEntity<Mono<Resposta>> alimentosIncorretos(AlimentosIncorretosException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getResposta());
    }
}
