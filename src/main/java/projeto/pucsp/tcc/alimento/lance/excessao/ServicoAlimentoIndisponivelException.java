package projeto.pucsp.tcc.alimento.lance.excessao;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import projeto.pucsp.tcc.alimento.lance.entidade.Resposta;
import reactor.core.publisher.Mono;

import java.net.ConnectException;

@Slf4j
@Getter
public class ServicoAlimentoIndisponivelException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -3629781590337896863L;

    private final transient Mono<Resposta> resposta;

    private final transient HttpStatus httpStatus;

    public ServicoAlimentoIndisponivelException(ConnectException e) {
        super(e);
        httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        resposta = Mono.just(Resposta.RESPOSTA_SERVICO_ALIMENTO_INDISPONIVEL);
    }

}
