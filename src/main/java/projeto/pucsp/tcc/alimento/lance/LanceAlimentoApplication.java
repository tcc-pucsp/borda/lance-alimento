package projeto.pucsp.tcc.alimento.lance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import projeto.pucsp.tcc.alimento.lance.propriedade.PropriedadeAlimento;
import projeto.pucsp.tcc.alimento.lance.propriedade.PropriedadeLanceAlimento;

@EnableConfigurationProperties({ PropriedadeLanceAlimento.class, PropriedadeAlimento.class })
@SpringBootApplication
public class LanceAlimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LanceAlimentoApplication.class, args);
	}

}
