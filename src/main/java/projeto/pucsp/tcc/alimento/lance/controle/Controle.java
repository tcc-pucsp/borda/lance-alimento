package projeto.pucsp.tcc.alimento.lance.controle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projeto.pucsp.tcc.alimento.lance.RecursoLanceAlimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.servico.ServicoLanceAlimentoProxy;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/publicar")
public class Controle implements RecursoLanceAlimento {

    private final RecursoLanceAlimento servico;

    public Controle(ServicoLanceAlimentoProxy servico) {
        this.servico = servico;
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    @Override
    public Mono<Void> publicarLance(@RequestHeader("id-social") Integer idSocial,
                                    @RequestHeader("email-social") String emailSocial,
                                    @RequestHeader("latitude") String latitude,
                                    @RequestHeader("longitude") String longitude,
                                    @RequestBody List<Alimento> alimentos
    ) {

        log.info("({},{}, {}, {}) Realiza Pedido de lance de alimentos : {}", idSocial, emailSocial,
                latitude, longitude, alimentos);

        return servico.publicarLance(idSocial, emailSocial, latitude, longitude, alimentos);

    }
}
