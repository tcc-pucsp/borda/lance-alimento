package projeto.pucsp.tcc.alimento.lance.cliente.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import projeto.pucsp.tcc.alimento.lance.cliente.ClienteAlimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.propriedade.PropriedadeAlimento;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.util.List;

@Slf4j
@Component
public class ClienteAlimentoImpl implements ClienteAlimento {

    private final WebClient webClient;

    private final PropriedadeAlimento propriedadeAlimento;

    public ClienteAlimentoImpl(WebClient webClient, PropriedadeAlimento propriedadeAlimento) {
        this.webClient = webClient;
        this.propriedadeAlimento = propriedadeAlimento;
    }

    @Override
    public Flux<Alimento> obterTodosAlimentosPorCodigos(List<Integer> codigos) {

        final String url = criarUrlString(codigos);

        log.info("realizando chamada HTTP::GET [{}] ", url);

        return webClient
                .get()
                .uri(URI.create(url)).header("Content-Type", "application/json")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::responseError)
                .bodyToFlux(Alimento.class);

    }

    private String criarUrlString(List<Integer> codigos) {

        String codigosString = codigos
                .stream()
                .map(Object::toString)
                .reduce((t, u) -> t.concat(",").concat(u))
                .orElse("");

        return String.format(propriedadeAlimento
                        .getServicoUrl()
                        .concat("/alimentos?codigo=%s"),
                codigosString);

    }

}
