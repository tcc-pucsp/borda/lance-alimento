package projeto.pucsp.tcc.alimento.lance.entidade;

import lombok.Value;

@Value
public class Localizacao {

    String latitude;

    String longitude;

}
