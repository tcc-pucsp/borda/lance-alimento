package projeto.pucsp.tcc.alimento.lance.dominio;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.alimento.lance.cliente.ClienteAlimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import projeto.pucsp.tcc.alimento.lance.excessao.AlimentosIncorretosException;
import projeto.pucsp.tcc.alimento.lance.excessao.CodigoAlimentosIncorretosException;
import projeto.pucsp.tcc.alimento.lance.excessao.ServicoAlimentoIndisponivelException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.ConnectException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AvaliadorAlimento {

    private final ClienteAlimento clienteAlimento;

    public AvaliadorAlimento(ClienteAlimento clienteAlimento) {
        this.clienteAlimento = clienteAlimento;
    }

    public Mono<Long> alimentosValidos(List<Alimento> alimentos) {

        log.info("Chamada para cliente de alimentos ... ");

        final List<Integer> codigos = alimentos
                .stream()
                .filter(Alimento::valido)
                .map(Alimento::getCodigo)
                .collect(Collectors.toList());

        return Flux
                .fromIterable(alimentos)
                .count()
                .filter(quantidade -> quantidade == codigos.size())
                .switchIfEmpty(Mono.error(new AlimentosIncorretosException(alimentos)))
                .then(
                        clienteAlimento
                                .obterTodosAlimentosPorCodigos(codigos)
                                .onErrorResume(ConnectException.class, e -> Mono.error(new ServicoAlimentoIndisponivelException(e)))
                                .map(Alimento::getCodigo)
                                .filter(codigos::contains)
                                .count()
                                .filter(quantidade -> quantidade == codigos.size())
                                .switchIfEmpty(Mono.error(new CodigoAlimentosIncorretosException(codigos)))
                );

    }
}
