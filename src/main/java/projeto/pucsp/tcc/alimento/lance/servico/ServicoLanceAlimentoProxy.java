package projeto.pucsp.tcc.alimento.lance.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.alimento.lance.RecursoLanceAlimento;
import projeto.pucsp.tcc.alimento.lance.dominio.AvaliadorAlimento;
import projeto.pucsp.tcc.alimento.lance.entidade.Alimento;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Component
public class ServicoLanceAlimentoProxy implements RecursoLanceAlimento {

    private final RecursoLanceAlimento servicoLanceAlimento;

    private final AvaliadorAlimento avaliadorAlimento;

    public ServicoLanceAlimentoProxy(ServicoLanceAlimento servicoLanceAlimento, AvaliadorAlimento avaliadorAlimento) {
        this.servicoLanceAlimento = servicoLanceAlimento;
        this.avaliadorAlimento = avaliadorAlimento;
    }

    @Override
    public Mono<Void> publicarLance(Integer idSocial,
                                    String emailSocial, String latitude,
                                    String longitude, List<Alimento> alimentos) {

        return avaliadorAlimento
                .alimentosValidos(alimentos)
                .flatMap(aBoolean -> servicoLanceAlimento.publicarLance(idSocial, emailSocial, latitude, longitude, alimentos));

    }

}
