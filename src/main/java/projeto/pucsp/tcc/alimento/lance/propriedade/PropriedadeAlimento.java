package projeto.pucsp.tcc.alimento.lance.propriedade;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ConfigurationProperties(prefix = "alimento")
@Validated
public class PropriedadeAlimento {

	private String servicoUrl;

}
